import * as React from 'react';

const Lead = ({solo_lead = {}, style, location, service}) => (
    <div id="js-lead-details-container" className={`col-lg-6 col-md-4 order-md-2  ${style}`}>
        <h2>Lead Details</h2>
        <h3>
            {solo_lead.name} is looking for {service} in {location}
        </h3>
        <p>Call: {solo_lead.phone}</p>
        <p>Email: {solo_lead.email}</p>
        <p>{solo_lead.more_info}</p>                          
    </div>
)
export default Lead;