import * as React from 'react';
  
class LeadList extends React.Component{
    constructor(){
        // Need a single source of truth / global store.
        super();
        this.state = {
            apiHost: 'http://localhost:4000',
            soloLead: {}
        }
    }

    getLeadFromApi = (id) => {
        return fetch(
            `${this.state.apiHost}/api/leads/${id}`,
            {
                method: 'GET'
            }
        )
    }

    viewSoloLead = (lead) => {
        this.getLeadFromApi(lead.id)
            .then(response => response.json())
            .then(soloLead => this.setState({soloLead}));
    }


    render () {
        const {soloLead} = this.state;
        const {lead} = this.props;
        return (
            <div>
                <li key={lead.id}>{lead.name} is looking for {lead.service_id} in {lead.location_id}</li>
                <button onClick={() => this.viewSoloLead(lead)} className="btn-link">More Details</button>
            </div>
        )
    }
}

export default LeadList;