import * as React from 'react';
import TitleSection from '../components/TitleSection';
import Dropdown from '../components/Dropdown';
import InputGroup from '../components/InputGroup';
import TextAreaGroup from '../components/TextAreaGroup';
import SubmitButton from '../components/SubmitButton';
import IconButton from '../components/IconButton';

class Enquiries extends React.Component{ 
    constructor(){
        super();
        this.state = {
            apiHost: 'http://localhost:4000',
            leadSuccessfulModalOpen: false,
            formData: {
                service_id: '',
                location_id: '',
                name: '',
                email: '',
                phone: '',
                more_info: ''
            },
            locationList: [],
            serviceList: []
        }
    }

    /**
     * Collection of APIs used in the file
     */
    getAllLocationsFromApi = () => fetch(`${this.state.apiHost}/api/locations`);
    getAllServicesFromApi = () => fetch(`${this.state.apiHost}/api/services`);

    /**
     * Converts an array to an object
     * @param {*} data 
     */
    arrayToObject = (data) => {
        const formatted = {};

        for(let i = 0; i < data.length; ++i) {
            const key = data[i].value;
            const value = data[i].text;
            formatted[key] = value;
        }
        return formatted;
    }

    /**
     * Uses the value from the input and the
     * string as the first parameter as the key
     * and updates the state for selected
     * @param {string} key 
     * @param {*} value 
     */
    updateValue = (key, value) => {
        const formData = this.state.formData;
        formData[key] = value;
        this.setState({formData});
    };

    /**
     * Adds all the data form the form into a POST request
     * and triggers the lead success modal to open
     */
    submitForm = () => {
        fetch(`${this.state.apiHost}/api/leads`, {
            method: 'POST',
            body: JSON.stringify(this.state.formData)
        }).then((response) => {
            this.setState({leadSuccessfulModalOpen: true}); 
        }).catch((e) => {
            console.log("error : ", e);
        });

    }

    /**
     * Called when the user exits the modal.
     * This resets the form data and sets the
     * modal open to false
     */
    closeModal = () => {
        this.setState({
            leadSuccessfulModalOpen: false,
            formData: {
                service_id: '',
                location_id: '',
                name: '',
                email: '',
                phone: '',
                more_info: ''
            },
        });
    }

    componentDidMount() {
        Promise.all([
            this.getAllLocationsFromApi(),
            this.getAllServicesFromApi()
        ]).then(async (data) => {
            this.setState({
                locationList: this.arrayToObject(await data[0].json()),
                serviceList: this.arrayToObject(await data[1].json())
            })
        });
    }

    render(){
        return (
            <div className="container">
                <section id="page">
                    <div className="row">
                        <TitleSection 
                            title={"Find the perfect Professional for you"}
                            sub_title={"Get free quotes within minutes"}
                        />
                        <div className="col-xl-6 col-md-9">       
                            <Dropdown 
                                title={"What Service area are you looking for"}
                                __update={value => this.updateValue('service_id', value)}
                                value={this.state.formData.service_id}
                                options={this.state.serviceList}
                                aria_label={"serviceDescription"}
                                placeholder={"Enter a service (Personal Trainer etc.)"}
                                extra_description={"We'll find the right Professionals for you"}
                            />
                            <Dropdown 
                                title={"Where are you looking?"}
                                __update={value => this.updateValue('location_id', value)}
                                value={this.state.formData.location_id}
                                options={this.state.locationList}
                                aria_label={"locationDescription"}
                                placeholder={"Enter a location (London etc.)"}
                            />
                            <InputGroup 
                                title={"Your Name"}
                                required={true}
                                __update={value => this.updateValue('name', value)}
                                value={this.state.formData.name}
                                form_name={"name"}
                                placeholder={"Enter Name"}
                            />
                            <InputGroup 
                                title={"Email Address"}
                                required={true}
                                form_type={"email"}
                                __update={value => this.updateValue('email', value)}
                                value={this.state.formData.email}
                                form_name={"email"}
                                aria_label={"emailDescription"}
                                placeholder={"Enter Email"}
                                extra_description={"We'll let you know when we've got Professionals for you"}
                            />
                            <InputGroup 
                                title={"Telephone"}
                                required={true}
                                form_type={"tel"}
                                __update={value => this.updateValue('phone', value)}
                                value={this.state.formData.phone}
                                form_name={"phone"}
                                aria_label={"telephoneDescription"}
                                placeholder={"Enter Telephone"}
                                extra_description={"So we can verify your information"}
                            />
                            <TextAreaGroup 
                                text={"Any Extra Information?"}
                                required={false}
                                __update={value => this.updateValue('more_info', value)}
                                value={this.state.formData.more_info}
                                form_name={"extra"}
                                aria_label={"extraInfoDescription"}
                                extra_description={"Include as much information as you can, so we can find the best Professionals"}
                            />
                            <SubmitButton 
                                cta={"Find Professionals"}
                                __click={this.submitForm}
                            />
                        </div>
                    </div>
                </section>
                { this.state.leadSuccessfulModalOpen && 
                    <div className="new-lead-modal">
                        <div className="modal-dialog modal-confirm">
                            <div className="modal-content">
                                <div className="modal-header text-white text-center">
                                    <button type="button" onClick={this.closeModal} className="close text-md" aria-hidden="true">×</button>
                                </div>
                                <div className="modal-body text-center">
                                    <h4>Great!</h4>
                                    <p>We've submitted your lead, you will hear from one of our Professionals soon.</p>
                                    <button onClick={this.closeModal} className="btn btn-primary" >
                                        <span>Submit another request</span>
                                        <IconButton icon="arrow-right"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}
export default Enquiries;