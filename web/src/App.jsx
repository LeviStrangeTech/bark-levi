import React from "react";
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import Header from './Header';
import Professionals from './Professionals';
import Enquiries from './Enquiries';
import './App.scss';

export default function App() {
  library.add(faSearch, faArrowRight);
  return (
    <div className="App">
      <Header/>
      <Route exact path="/">
        <Enquiries/>
      </Route>
      <Route exact path="/professionals">
        <Professionals/>
      </Route>  
    </div>
  );
}