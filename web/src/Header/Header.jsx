import * as React from 'react';
  
const Header = () => (
    <nav className="navbar sticky-top navbar-expand bark-header body-min-width header-shadow background-color-white py-0" id="bark-header">
        <div className="container">
            <div>
                <a className="navbar-brand my-3" href="/">
                <img className="img-fluid" src="https://d18jakcjgoan9.cloudfront.net/s/img/images/barklogo-dark.png!d=KY4fXZ" width="106" height="32" title="Bark Logo" alt="Bark Logo"/>
                </a>
                <a className="px-3" href="/">Customers</a>
                <a className="px-3" href="/professionals">Professionals</a>
            </div>
        </div>
    </nav>
);
export default Header;