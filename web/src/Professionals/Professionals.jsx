import * as React from 'react';
import TitleSection from '../components/TitleSection';
import Dropdown from '../components/Dropdown';
import Lead from '../LeadList/Lead';
import IconButton from '../components/IconButton';

class Professionals extends React.Component{
    constructor(){
        super();
        this.state = {
            apiHost: 'http://localhost:4000',
            leadList: [],
            locationList: [],
            serviceList: [],
            selected : {
                service: '',
                location: ''
            },
            soloLead: {},
            location: {},
            service: {}
        }
    } 

    /**
     * Collection of APIs used in the file
     */
    getAllLeadsFromApi = () => fetch(`${this.state.apiHost}/api/leads`);
    getFilteredLeadsFromApi = (location, service) => fetch(`${this.state.apiHost}/api/leads?location_id=${location}&service_id=${service}`);
    getAllLocationsFromApi = () => fetch(`${this.state.apiHost}/api/locations`);
    getAllServicesFromApi = () => fetch(`${this.state.apiHost}/api/services`);
    getLeadFromApi = (id) => fetch(`${this.state.apiHost}/api/leads/${id}`);
    getLocationFromApi = (id) => fetch(`${this.state.apiHost}/api/locations/${id}`)
    getServiceFromApi = (id) => fetch(`${this.state.apiHost}/api/services/${id}`)
    
    /**
     * Converts an array to an object
     * @param {*} data 
     */
    arrayToObject = (data) => {
        const formatted = {};

        for(let i = 0; i < data.length; ++i) {
            const key = data[i].value;
            const value = data[i].text;
            formatted[key] = value;
        }
        return formatted;
    }

    /**
     * Uses the lead id to make an api call
     * to the single lead which appears as the 
     * lead details
     * @param {object} lead 
     */
    viewSoloLead = (lead) => {
        this.setState({soloModalVisible: true});
        this.getLeadFromApi(lead.id)
            .then(response => response.json())
            .then(soloLead => this.setState({soloLead}));
    }

    /**
     * Uses the value from the input and the
     * string as the first parameter as the key
     * and updates the state for selected
     * @param {string} key 
     * @param {*} value 
     */
    updateValue = (key, value) => {
        const selected = this.state.selected;
        selected[key] = value;
        this.setState({selected});
    };

    /**
     * Uses the location id and service id
     * to query the API and return a filtered 
     * list of leads
     * @param {*} location 
     * @param {*} service 
     */
    updateList = (location, service) => {
        this.getFilteredLeadsFromApi(location, service)
        .then(async (data) => {
            this.setState({leadList: await data.json()})
        });
    };

    componentDidMount() {
        Promise.all([
            this.getAllLeadsFromApi(),
            this.getAllLocationsFromApi(),
            this.getAllServicesFromApi()
        ]).then(async (data) => {
            this.setState({
                leadList: await data[0].json(),
                locationList: this.arrayToObject( await data[1].json()),
                serviceList: this.arrayToObject( await data[2].json())
            })
        });
    }

    render(){
        const {
            leadList, 
            soloLead, 
            soloModalVisible, 
            serviceList, 
            locationList,
            selected,
        } = this.state;

        const allLeads = leadList.map((lead) => {
            return (
                <div className="lead-list-item" key={lead.id}>
                    <li>{lead.name} is looking for {serviceList[lead.service_id]} in {locationList[lead.location_id]}</li>
                    <button onClick={() => this.viewSoloLead(lead)} className="btn btn-link">More Details</button>
                </div>
            );
        });

        return (
            <div className="container">
                <section id="page">
                    <div className="row">
                        <div className="col-12">
                            <div className="row">
                                <TitleSection 
                                    title={"Find the right customers for your business"}
                                    sub_title={"Every six seconds a customer searches for a service on Bark"}
                                />
                            </div>
                            <div className="row">
                                <div className="col-xl-6 col-md-6">
                                    <Dropdown 
                                        title={"What service do you provide?"}
                                        __update={value => this.updateValue('service', value)}
                                        value={selected.service}
                                        options={serviceList}
                                        placeholder={"e.g. Personal Trainers"}
                                    />
                                    <Dropdown 
                                        title={"Where do you provide it?"}
                                        __update={value => this.updateValue('location', value)}
                                        value={selected.location}
                                        options={locationList}
                                        placeholder={"e.g. London"}
                                    />             
                                    <div className="form-group col-auto">
                                        <IconButton 
                                            __filter={() => this.updateList(selected.location, selected.service)}
                                            icon="search"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <Lead 
                                    solo_lead={soloLead}
                                    location={locationList[soloLead.location_id]}
                                    service={serviceList[soloLead.service_id]}
                                    style={soloModalVisible ? "" : "d-none"}
                                />
                                <div className="col-lg-6 col-md-8 order-md-1">
                                    <h2>Your Leads</h2>
                                    {allLeads}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
export default Professionals;