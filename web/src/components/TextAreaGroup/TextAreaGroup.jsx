import * as React from 'react';
  
const TextAreaGroup = ({title, required, form_name, aria_label = '', placeholder, extra_description = '', __update = () => null}) => (
    <div className="form-group">
        <label>{title}</label>
        <textarea 
            className="form-control" 
            name={form_name}
            required={required} 
            aria-describedby={aria_label} 
            placeholder={placeholder} 
            rows="3"
            onChange={e => __update(e.target.value)}
        />
        <small id="extraInfoDescription" className="form-text text-muted">
            {extra_description}
        </small>
    </div>
);
export default TextAreaGroup;