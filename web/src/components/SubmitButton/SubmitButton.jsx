import * as React from 'react';
  
const SubmitButton = ({cta, __click = () => {}}) => (
    <div className="form-group">
        <button onClick={__click} className="btn btn-primary">
            {cta}
        </button>
    </div>
);
export default SubmitButton;