import * as React from 'react';
  
const Dropdown = ({title, aria_label, placeholder, extra_description = '', value, options = {}, __update = () => null}) => {
    const optionList = [<option value=''>Select from dropdown</option>];
    for (let value in options) {
        optionList.push(<option value={value}>{options[value]}</option>)
    }
    return (
        <div className="form-group">
            <label>{title}</label>
            <select 
                className="form-control"
                placeholder={placeholder}
                aria-describedby={aria_label}
                value={value}
                onChange={e => __update(e.target.value)}
            >
                {optionList}
            </select>
            <small className="form-text">
                {extra_description}
            </small>
        </div>
    );
};
export default Dropdown;