import * as React from 'react';
  
const TitleSection = ({title, sub_title = ''}) => (
    <div className="col-12">
        <h1 className="mt-2">{title}</h1>
        <h4 className="text-light-grey mb-4">{sub_title}</h4>
    </div>
);
export default TitleSection;