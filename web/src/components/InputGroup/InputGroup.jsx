import * as React from 'react';
  
const InputGroup = ({title, required, form_type, form_name, aria_label = '', placeholder, extra_description = '', value, __update = () => null}) => (
    <div className="form-group">
        <label>{title}</label>
        <input 
            required={required} 
            type={form_type ? form_type : 'text'} 
            className="form-control" 
            name={form_name}
            aria-describedby={aria_label} 
            placeholder={placeholder} 
            value={value} 
            onChange={e => __update(e.target.value)}
        />
        <small className="form-text text-muted">
            {extra_description}
        </small>
    </div>
);
export default InputGroup;