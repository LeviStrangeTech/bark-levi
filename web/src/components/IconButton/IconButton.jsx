import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const IconButton = ({type = "", id = "", icon, __filter = () => null}) => (
    <div className="col-auto">
        <button 
            type={type} 
            className="btn" 
            id={id}
            onClick={__filter()}
        >
            <FontAwesomeIcon icon={icon}/>
        </button>
    </div>
);
export default IconButton;