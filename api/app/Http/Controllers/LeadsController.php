<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\Location;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Validator;


class LeadsController extends Controller
{

    /**
     * Find the nearest city based on the provided lat / lng
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse {
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'name' => 'required|max:200',
            'email' => 'required|max:200',
            'phone' => 'required|max:100',
            'more_info' => 'max:1000',
            'location_id' => 'required|integer',
            'service_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()
            ]);

        }

        $input = [
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'more_info' => array_key_exists('more_info', $data) ? $data['more_info'] : '',
            'location_id' => $data['location_id'],
            'service_id' => $data['service_id'],
        ];

        Lead::create($input);

        return response()->json([
            'status'    =>  true
        ]);
    }

    public function all(Request $request) {
        $lead = Lead::query();

        if ($request->has('location_id') && $request->location_id !== null) {
            $lead->where('location_id', '=', $request->location_id);
        }

        if ($request->has('service_id') && $request->service_id !== null) {
            $lead->where('service_id', '=', $request->service_id);
        }
        // add a QS param which gets all locations with id/services with id return list.
        return $lead->get();
        
    }

    public function one($id) {
        $lead = Lead::findOrFail($id);
        $lead->name = explode(' ', $lead->name)[0];
        $obfuscateEmail = '/^(\w).*?(\w)+@(\w).*?(\w)(\.[\w.]+)$/i';
        $lead->email = preg_replace($obfuscateEmail, '$1****$2@$3****$4$5', $lead->email);
        $lead->phone = substr_replace($lead->phone, '*****', 0,5);
        return $lead;
    }
}
